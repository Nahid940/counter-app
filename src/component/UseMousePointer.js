import React,{Component} from 'react'

class UseMousePointer extends Component
{
    constructor(props)
    {
        super(props)
    }

    render()
    {
    //    const {x,y,mousePointerHanlder}=this.props
        return 
        {
            <div onMouseMove={this.props.mousePointerHanlder}>
                This is Component using mouse Use Mouse Pointer
                The x is {this.props.x} from use component and y is {this.props.y} from use component
            </div>
        }
    }
}
export default UseMousePointer