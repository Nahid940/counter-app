import React,{Component} from 'react'

class MousePointer extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
            x:0,
            y:0
        }
    }

    mousePointerHanlder=(e)=>
    {
        this.setState({
            x:e.clientX,
            y:e.clientY
        })
    }

    render ()
    {
        return (
            // <div  onMouseMove={this.mounPointerHanlder}>
            //     The X point is{this.state.x} and Y point is {this.state.y}
            // </div>
            <div>
                {this.props.render(this.state.x,this.state.y,this.mousePointerHanlder)}
            </div>
        )
    }


}

export default MousePointer