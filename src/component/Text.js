import React ,{Component} from 'react'

class Text extends Component
{
    constructor(props)
    {
        super(props)

    }

    render()
    {
        const {text,hoverHandler}=this.props
        return(
            <p onMouseOver={hoverHandler}>Hover Here {text}</p>
        )
    }
}

export default Text