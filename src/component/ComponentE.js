import React,{Component} from 'react'

import ComponentF from './ComponentF'

import {UserConsumer} from './UserContext'

class ComponentE extends Component 
{
    constructor(props)
    {
        super(props)
    }

    render()
    {
        return (

            <UserConsumer>
                {
                    (name)=>{
                        return (
                            <div>
                                <p>The Context is {name} E</p>

                                <ComponentF/>
                            </div>
                        )
                    }
                }
            </UserConsumer>
        )
    }
}

export default ComponentE