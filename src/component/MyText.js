import React, {Component } from "react";

class MyText extends Component
{
    constructor(props)
    {
        super(props)

        this.state={
            text:''
        }
    }

    hoverHandler=()=>{
        this.setState(prevState=>{return {text:prevState.text="Mouse hovered on the Text"}})
    }

    render()
    {
        return(
            <div>
                {this.props.render(this.state.text,this.hoverHandler)}
            
            </div>
        )
    }

}

export default MyText