import React ,{Component} from 'react'



class HoverComponent extends Component
{
    
    render()
    {
        const {count,handleCounter}=this.props

        return (
            <div>
                Hover <h1 onMouseOver={handleCounter}> Hovered {count} times</h1>
            </div>
        )
    }
}

export default HoverComponent