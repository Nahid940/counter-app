import React, { PureComponent } from 'react'
import { directive } from '@babel/types'

class Parent extends PureComponent {
    
    render() {
        const {count,handleCounter}=this.props
        return (
            <div>
                <button onClick={handleCounter}>Clicked {count} times</button>
            </div>
        )
    }
}

export default Parent