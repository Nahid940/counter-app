import React,{Component} from 'react'
import { directive } from '@babel/types'

class Counter extends Component
{
    constructor(props)
    {
        super(props)

        this.state={
            count:0
        }
    }

    hoverCounter=()=>{
        this.setState(prevState=>{return {count:prevState.count+1}})
    }

    render()
    {
        return (
            <div>
                {this.props.render(this.state.count,this.hoverCounter)}
            </div>
        )
    }
}

export default Counter