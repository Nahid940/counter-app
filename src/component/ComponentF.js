import React,{Component} from 'react'

import {UserConsumer} from './UserContext'

class ComponentF extends Component 
{
    // constructor(props)
    // {
    //     super(props)
    // }
    // <h1>Component F and the context value is {username=>{return <div>{username}</div>}}</h1>
    render()
    {
        return (
            <UserConsumer>
                {
                    (username)=>{
                        return <div>
                            Hello {username} F
                            
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing
                                 elit. Hic
                                 architecto fuga temporibus consequatur rerum 
                                 qui adipisci autem voluptatem, dicta,
                                 at ipsa animi officia mollitia enim pariatur
                                  impedit natus quis iure.</p>
                            </div>
                    }
                }
            </UserConsumer>
            
        )
    }
}
export default ComponentF