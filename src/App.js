import React from 'react';
import './App.css';

import Parent from './component/Parent'
import HoverComponent from './component/HoverComponent'
import Counter from './component/Counter'
import Text from './component/Text'
import MyText from './component/MyText';

import ComponentD from './component/ComponentD'
import {UserProvider} from './component/UserContext'

import PostList from './component/PostList'

import MousePointer from './component/MousePointer'
// import UseMousePointer from './component/UseMousePointer'
import Counters from './counter/Counters'
function App() {
  return (
    <div className="App">

      {/* <Parent/>
      <HoverComponent/> */}
      {/* <Counter render={(count,handleCounter)=> (<Parent count={count} handleCounter={handleCounter}/>) }/> */}
      {/* <Counter render={(count,handleCounter)=> (<HoverComponent count={count} handleCounter={handleCounter}/> )}/> */}
     {/* <MyText render={(text,hoverHandler)=>(<Text text={text} hoverHandler={hoverHandler} />)} /> */}
    {/* <UserProvider value="React Context">
      <ComponentD/>
    </UserProvider> */}
    {/* <PostList/> */}

    {/* <MousePointer render={(x,y,mousePointerHanlder)=>(<UseMousePointer x={x} y={y}  mousePointerHanlder={mousePointerHanlder}/> )}/> */}

    <Counters/>
    </div>
  );
}

export default App;
