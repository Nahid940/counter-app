import React,{useState} from 'react'
import Counter from './Counter'
import Form from './Form'
const Counters=()=>
{

    const [data,setData]=useState([
        {
            id:1,
            value:1
        },
        {
            id:2,
            value:0
        },
        {
            id:3,
            value:2
        },
        {
            id:4,
            value:0
        },
    ])
    
    const [formdata,setFormdata]=useState({
        id:0,
        value:''
    })

    const handleInput=(event)=>{
        setFormdata({
            ...formdata,[event.target.name]:event.target.value
        })
    }

    const submitHandler=(event,prevState)=>{
        event.preventDefault()
        // const datalists={
        //     id:formdata.id,
        //     value:formdata.value
        // }

        setData([
            ...data,formdata
        ])
        formdata.id=(Number(formdata.id)+1)
    }

    const handleReset=()=>{
        const resetData=data.map(d=>{
            d.value=0;
            return d;
        })
        setData(resetData)
    }



    const removeItem=(id)=>
    {
        const newList=data.filter(d=>(d.id!=id))
        setData(newList)
    }

    const incrementData=(datas)=>
    {
        const newdata=[...data]
        const index=newdata.indexOf(datas)
        newdata[index].value++

        setData(
            newdata
        )
        // console.log(index)
    }

    return(
        <React.Fragment>
            <div className="col-md-3">
                <button className="btn btn-warning btn-sm" onClick={handleReset}>Reset</button>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Quantity</th>
                            <th>Status</th>
                            <th>Increment</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    {data.map(d=>(
                        <Counter data={d} removeItem={removeItem} incrementData={incrementData}/>
                    ))}
                </table>
            </div>
            <Form formdata={formdata} handleInput={handleInput} submitHandler={submitHandler}/>
        </React.Fragment>
    )

}

export default Counters