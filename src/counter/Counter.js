import React,{useState} from 'react'

const Counter=(props)=>
{

    let badge="badge ";
    let status='';

    if(props.data.value==0)
    {
        badge+=" badge-danger";
        status="Empty"

    }else
    {
        badge+="badge-info"
        status="Good"
    }

    return (
        <React.Fragment>

            <tbody>
                <tr>
                    <td>{props.data.value}</td>
                    <td><span className={badge}>{status}</span></td>
                    <td><button className="btn btn-success btn-sm" onClick={()=>props.incrementData(props.data)}>+</button></td>
                    <td><button className="btn btn-danger btn-sm" onClick={()=>props.removeItem(props.data.id)}>-</button></td>
                </tr>
            </tbody>

        </React.Fragment>
    )

}

export default Counter