import React,{useState} from 'react'

const Form =(props)=>{
    
    return (
        <div>
            <div class="col-md-3">
                <form onSubmit={props.submitHandler}>
                    <input type="text" name="id" className="form-control" placeholder="ID" value={props.formdata.id} onChange={props.handleInput}/>
                        &nbsp;
                    <input type="text" name="value" className="form-control" placeholder="Value" value={props.formdata.value} onChange={props.handleInput}/>

                    <button type='submit' className="btn btn-success btn-sm ">Add</button>
                </form>
            </div>
        </div>
    )
}

export default Form